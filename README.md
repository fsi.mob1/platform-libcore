Finch-Platform: AOSP platform/libcore mod
=========================================

## NOTICE

This project is part of [Finch](https://gitlab.com/fsi.mob1/finch); please
see earlier link for entire project description.

We may missed some requirements such as mentioning correct authors of our
dependent library, and so on. We never tries to violate other's, so if you
found mistake please feel free to contact us and we will gladly correct them.

## AUTHORS

Original works from https://android.googlesource.com/platform/libcore .

Modification tracked in this repository are made by belowing author(s):

- SungHyoun Song <decash@fsec.or.kr>

## LICENSE

GPL 2.0. See [LICENSE](LICENSE) for details.

Note that our modification follows original file's license to avoid incompatible
licenses between GPLv2 and Apache 2.0 in original repository. For example,
platform/libcore basically uses GPLv2, but [Android.mk](Android.mk) which appended
when AOSP created uses Apache 2.0. We obey original author's license agreements.

You can see modification by diffing branch `master` and other named branch.
